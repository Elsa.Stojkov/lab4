package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    private int rows;
    private int cols;
    private CellState[][] cellGrid;

    public CellGrid(int rows, int columns, CellState initialState) {
        this.rows = rows;
        this.cols = columns;
        this.cellGrid = new CellState[rows][cols];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                cellGrid[i][j] = initialState;
            }
        }
	}

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return cols;
    }

    private void checkValid(int n, int maxN) {
        if (n < 0 || n > maxN) {
            throw new IndexOutOfBoundsException();
        }
    }

    @Override
    public void set(int row, int column, CellState element) {
        checkValid(row, numRows());
        checkValid(column, numColumns());
        cellGrid[row][column] = element;
    }

    @Override
    public CellState get(int row, int column) {
        checkValid(row, numRows());
        checkValid(column, numColumns());
        return cellGrid[row][column];
    }

    @Override
    public IGrid copy() {
        CellGrid copiedGrid = new CellGrid(numRows(), numColumns(), null);
        for (int i = 0; i < numRows(); i ++) {
            for (int j = 0; j < numColumns(); j++) {
                copiedGrid.set(i, j, get(i, j));
            }
        }
        return copiedGrid;
    }
}
