package cellular;

import java.util.Random;
import datastructure.CellGrid;
import datastructure.IGrid;

public class BriansBrain implements CellAutomaton {

    
	IGrid currentGeneration;
	int height;
	int width;

	public BriansBrain(int rows, int columns) {
		this.height = rows;
		this.width = columns;
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		
		initializeCells();
	}

    @Override
    public CellState getCellState(int row, int column) {
        return currentGeneration.get(row, column);
    }

    @Override
    public void initializeCells() {
        Random random = new Random();
        for (int row = 0; row < currentGeneration.numRows(); row++) {
            for (int col = 0; col < currentGeneration.numColumns(); col++) {
                if (random.nextBoolean()) {
                    currentGeneration.set(row, col, CellState.ALIVE);
                } else if (random.nextBoolean()) {
                    currentGeneration.set(row, col, CellState.DYING);
                } else {
                    currentGeneration.set(row, col, CellState.DEAD);
                }
            }
        }       
    }

    @Override
    public void step() {
        IGrid nextGeneration = currentGeneration.copy();
		// TODO
		for (int row = 0; row < height; row++) {
			for (int col = 0; col < width; col ++) {
				nextGeneration.set(row, col, getNextCell(row, col));
			}
		}
		currentGeneration = nextGeneration;
	}    
    

    @Override
    public CellState getNextCell(int row, int col) {
		int alive = countNeighbors(row, col, CellState.ALIVE);
		if (currentGeneration.get(row, col) == CellState.DEAD) {
			return (alive == 2) 
				? CellState.ALIVE 
				: CellState.DEAD;
		}
		return (currentGeneration.get(row, col) == CellState.ALIVE) 
			? CellState.DYING
			: CellState.DEAD;
	}

	private int countNeighbors(int row, int col, CellState state) {
		int[][] neighbors = {
			{row + 1, col - 1}, {row + 1, col}, 
			{row + 1, col + 1}, {row, col + 1},
			{row - 1, col + 1}, {row - 1, col}, 
			{row - 1, col - 1}, {row, col - 1}
		};
		int counter = 0;
		for (int[] neighbor : neighbors) {
			try {
				if (currentGeneration.get(neighbor[0], neighbor[1]) == state) {
					counter ++;
				}
			} catch (IndexOutOfBoundsException e) {
				continue;
			}
		}
		System.out.println(counter);
		return counter;
	}

    @Override
    public int numberOfRows() {
        return height;
    }

    @Override
    public int numberOfColumns() {
        return width;
    }

    @Override
    public IGrid getGrid() {
        return currentGeneration;
    }
    
}
